const ShortenController = require('../api/shorten/controller')
const {createApi, createConnection, closeConnection} = require('../test-helper')

describe('App Routes Test', () => {
    let api

    beforeAll(async () => {
        api = createApi()
        await createConnection()
    })

    afterAll(async () => {
        await closeConnection()
    })

    it('Health OK', async () => {
        const response = await api.get('/')
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual({shortenAPI: 'OK'})
    })

    it('should get error with code and message defined', async () => {
        const error = new Error('Link Not Found')
        error.status = 400
        const mockHealth = jest.fn().mockImplementation(ctx => {
            throw error
        })

        ShortenController.health = mockHealth

        const response = await api.get('/')
        expect(response.status).toBe(400)
        expect(response.text).toBe('Link Not Found')

        mockHealth.mockReset()
    })

    it('should get default error code when status not defined', async () => {
        const mockHealth = jest.fn().mockImplementation(ctx => {
            throw new Error('Internal Server Error')
        })

        ShortenController.health = mockHealth

        const response = await api.get('/')
        expect(response.status).toBe(500)
        expect(response.text).toBe('Internal Server Error')

        mockHealth.mockReset()
    })

    it('should create new shorten url', async () => {
        const data = {url: 'https://google.com.br'}
        const response = await api.post('/encurtador').send(data).set('Host', 'http://localhost:3000')
        const {newUrl} = response.body
        expect(newUrl).toBeDefined()
        expect(newUrl.includes('localhost:3000')).toBe(true)
    })

    it('should redirect to original url', async () => {
        const data = {url: 'https://google.com.br'}
        const response = await api.post('/encurtador').send(data).set('Host', 'http://localhost:3000')
        const {newUrl} = response.body
        const hash = newUrl.split('/').slice(-1)[0]
        const redirectResponse = await api.get(`/${hash}`)
        expect(redirectResponse.status).toBe(302)
        expect(redirectResponse.header['location']).toBe(data.url)
    })

    it('should get error when not inform url', async () => {
        const response = await api.post('/encurtador')
        expect(response.status).toBe(400)
        expect(response.text).toBe('You need to enter a url, to shorten it for you')
    })

    it('should get error when inform empty url', async () => {
        const response = await api.post('/encurtador').send({url:''})
        expect(response.status).toBe(400)
        expect(response.text).toBe('You need to enter a url, to shorten it for you')
    })
})