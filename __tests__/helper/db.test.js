const databaseConfig = require('../../config/database')

describe('Helper Db', () => {
    let db

    beforeEach(() => {
        db = require('../../helper/db')
    })

    it('should set config', () => {
        const config = {uri: 'mongo://uri/db'}
        db.setConfig({uri: 'mongo://uri/db'})
        expect(db.config).toStrictEqual(config)
    })

    it('should get database config and call connection', () => {
        const mockConnection = jest.fn()
        const mockSetConfig = jest.fn()
        const config = databaseConfig.getConfig()

        db.connect = mockConnection
        db.setConfig = mockSetConfig

        db.initialize()
        expect(mockSetConfig.mock.calls.length).toBe(1)
        expect(mockSetConfig.mock.calls[0][0]).toStrictEqual(config)

        expect(mockConnection.mock.calls.length).toBe(1)

        mockSetConfig.mockRestore()
        mockConnection.mockRestore()

    })
})