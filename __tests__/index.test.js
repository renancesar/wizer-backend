const app = require ('../app')
const db = require('../helper/db')

const mockListen = jest.fn()
const initializeDb = jest.fn()
app.listen = mockListen
db.initialize = initializeDb


afterEach(() => {
    mockListen.mockReset()
    initializeDb.mockReset()
});

test('Server works', async () => {
    require('../index')
    expect(mockListen.mock.calls.length).toBe(1)
    expect(mockListen.mock.calls[0][0]).toBe(process.env.PORT || 3000)
    expect(initializeDb.mock.calls.length).toBe(1)
});