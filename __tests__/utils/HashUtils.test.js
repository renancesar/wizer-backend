const HashUtils = require('../../utils/HashUtils')
const {times, uniq, every} = require('lodash')
describe('HashUtils Spec', () => {
    it('should create 100 uniq hashes', () => {
        const hashs = []
        times(100, () => {
            hashs.push(HashUtils.generateHash())
        })
        const uniqValues = uniq(hashs)
        expect(uniqValues.length).toBe(hashs.length)
    })

    it('should create 100 hashes with length between 5 - 10', () => {
        const hashs = []
        times(100, () => {
            hashs.push(HashUtils.generateHash())
        })
        const isAllValidHashs = every(hashs, hash => (hash.length >=5 && hash.length <= 10))

        expect(isAllValidHashs).toBe(true)
    })
})