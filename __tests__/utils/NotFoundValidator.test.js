const NotFoundError = require('../../utils/NotFoundValidator')

describe('NotFoundValidator Spec', () => {
    it('Should throw error when validation failed', () => {
        try {
            new NotFoundError(false, 'Error Message').assert()
        } catch (error) {
            expect(error.message).toBe('Error Message')
            expect(error.status).toBe(404)
        }
    })

    it('Should not throw error when validation not failed', () => {
        try {
            new NotFoundError(true, 'Error Message').assert()
            expect(true).toBe(true)
        } catch (error) {
            expect(false).toBe(true)
        }
    })
})