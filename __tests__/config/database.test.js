const databaseConfig = require('../../config/database')

describe('Database config', () => {
    let originalEnv, mongoUrl
    beforeAll(() => {
        originalEnv = process.env.NODE_ENV
        originalEnv = process.env.MONGO_URL
    })

    afterAll(() => {
        process.env.NODE_ENV = originalEnv
        process.env.MONGO_URL = mongoUrl
    })

    describe('Env dev', () => {
        it('should get config by env dev', () => {
            process.env.NODE_ENV = 'dev'
            delete process.env.MONGO_URL
            const config = databaseConfig.getConfig()
            expect(config.url).toBe('mongodb://localhost/wiser-shorten')
        })

        it('should get config dev when no has NODE_ENV defined', () => {
            delete process.env.NODE_ENV
            delete process.env.MONGO_URL
            const config = databaseConfig.getConfig()
            expect(config.url).toBe('mongodb://localhost/wiser-shorten')
        })
    })

    describe('Env test', () => {
        it('should get config by env test', () => {
            process.env.MONGO_URL = mongoUrl
            process.env.NODE_ENV = 'test'
            const config = databaseConfig.getConfig()
            expect(config.url).toBe(process.env.MONGO_URL)
        })

        it('should get config default test when no has MONGO_URL defined', () => {
            process.env.NODE_ENV = 'test'
            delete process.env.MONGO_URL
            const config = databaseConfig.getConfig()
            expect(config.url).toBe('mongodb://localhost/wiser-shorten-test')
        })
    })

    describe('Env production', () => {
        beforeEach(() => {
            process.env.NODE_ENV = 'production'
        })

        it('should get config by env production with MONGOHQ_URL', () => {
            delete process.env.MONGODB_URI
            process.env.MONGOHQ_URL = 'mongodb://production-link/wiser-prod'
            const config = databaseConfig.getConfig()
            expect(config.url).toBe(process.env.MONGOHQ_URL)
        })

        it('should get config by env production with MONGODB_URI', () => {
            delete process.env.MONGOHQ_URL
            process.env.MONGODB_URI = 'mongodb://production-link/wiser-prod-URI'
            const config = databaseConfig.getConfig()
            expect(config.url).toBe(process.env.MONGODB_URI)
        })
    })
})