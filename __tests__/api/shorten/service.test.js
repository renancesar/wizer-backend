const Service = require('../../../api/shorten/service')
const {createConnection, closeConnection} = require('../../../test-helper')

describe('Shorten Service Spec', () => {
    beforeAll(async () => {
        await createConnection()
    });

    afterAll(async () => {
        await closeConnection()
    });

    describe('.shorten',  () => {
        it('should create shorten and return new url',  async () => {
            const url = 'https://google.com.br'
            const host = 'localhost:3000'
            const response = await Service.shorten(url, host)
            expect(response.newUrl.includes('localhost:3000')).toBe(true)
        })

        it('should get original url by hash',  async () => {
            const url = 'https://google.com.br'
            const host = 'localhost:3000'
            const {hash} = await Service.shorten(url, host)
            const originalUrl = await Service.getByHash(hash)
            expect(originalUrl).toBe(url)
        })

        it('should return error when hash not found',  async () => {
            expect.assertions(2)
            try {
                const response = await Service.getByHash('HASH_NOT_FOUND')
            } catch (error) {
                expect(error.status).toBe(404)
                expect(error.message).toBe('Link not found')
            }

        })
    })
})