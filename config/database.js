const configs = () => ({
    dev: {
        url: `${process.env.MONGO_URL || 'mongodb://localhost'}/wiser-shorten`
    },
    test: {
        url: process.env.MONGO_URL || 'mongodb://localhost/wiser-shorten-test'
    },
    production: {
        url: process.env.MONGOHQ_URL || process.env.MONGODB_URI
    }
})

const getConfig = () => configs()[process.env.NODE_ENV || 'dev']

module.exports = { getConfig }