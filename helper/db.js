const databaseConfig = require('../config/database')
const mongoose = require('mongoose')
const Promise = require('bluebird')

const options = {
    keepAlive: true,
    keepAliveInitialDelay: 300000,
    connectTimeoutMS: 1800000,
    promiseLibrary: Promise,
    poolSize: 10,
    useFindAndModify: false,
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    socketTimeoutMS: 0
}

class Database {
    constructor(config) {
        this.config = config
        this.connection = {}
    }

    setConfig (config) {
        this.config = config
    }

    connect () {
        const self = this

        mongoose.connect(this.config.url, options, (err) => {
            if(err) console.error(err)
        })
        this.connection = mongoose.connection
        this.connection.once('open', () => console.info('Started mongoose connection'))
        this.connection.on('error', () => {
            console.info('MONGO ERROR, Trying reconnect')
            self.connection = mongoose.createConnection(this.config.url, options)
        })
    }

    initialize () {
        this.setConfig(databaseConfig.getConfig())
        this.connect()
    }
}

module.exports = new Database({})
