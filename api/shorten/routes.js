const Router = require('koa-router')
const controller = require('./controller')

const Route = Router()
    .post('/encurtador', (ctx) => controller.shorten(ctx))
    .get('/:hash', (ctx) => controller.redirect(ctx))
    .get('/', (ctx) => controller.health(ctx))

module.exports = Route
