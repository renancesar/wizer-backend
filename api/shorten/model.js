const mongoose = require ('mongoose')

const documentTTL = process.env.SHORTEN_TTL  || 3600

const ShortenSchema = mongoose.Schema({
    url: String,
    hash: String,
    newUrl: String,
}, {
    timestamps: true
})

ShortenSchema.index({createdAt: 1}, {expireAfterSeconds: Number(documentTTL)})

const Shorten = mongoose.model('shortens', ShortenSchema, 'shortens')

module.exports = Shorten