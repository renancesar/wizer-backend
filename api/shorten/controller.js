const Service = require('./service')

const ShortenController = {
    async shorten (ctx) {
        const {url} = ctx.request.body
        const host = ctx.headers.host
        const {newUrl} = await Service.shorten(url, host)
        ctx.body = {newUrl}
    },

    async redirect (ctx) {
        const {hash} = ctx.params
        const url = await Service.getByHash(hash)
        ctx.redirect(url)
    },

    async health (ctx) {
        ctx.body = {shortenAPI: 'OK'}
    }
}

module.exports = ShortenController