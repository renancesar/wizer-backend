const ShortenRepository = require('./repository')
const HashUtils = require('../../utils/HashUtils')
const NotFoundValidator = require('../../utils/NotFoundValidator')
const BadRequestValidator = require('../../utils/BadRequestValidator')
const ShortenService = {
    async shorten (url, host) {
        new BadRequestValidator(url, 'You need to enter a url, to shorten it for you').assert()
        const hash = HashUtils.generateHash(8)
        const newUrl = `${host}/${hash}`
        return ShortenRepository.create(url, hash, newUrl)
    },

    async getByHash (hash) {
        const doc = await ShortenRepository.getByHash(hash)
        new NotFoundValidator(doc, 'Link not found').assert()
        return doc.url
    }
}

module.exports = ShortenService