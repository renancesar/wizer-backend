const Shorten = require('./model')

const ShortenRepository = {
    async create (url, hash, newUrl) {
        return Shorten.create({url, hash, newUrl})
    },

    async getByHash (hash) {
        return Shorten.findOne({hash})
    }
}

module.exports = ShortenRepository