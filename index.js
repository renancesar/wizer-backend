const app = require('./app')
const db = require('./helper/db')

const PORT = process.env.PORT || 3000
db.initialize()
app.listen(PORT)