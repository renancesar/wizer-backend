class NotFoundValidator extends Error {
    constructor(data, message) {
        super(message);
        this.status = 404
        this.data = data
    }

    assert () {
        if (!this.data) {
            throw this
        }
    }
}

module.exports = NotFoundValidator