class BadRequestValidator extends Error {
    constructor(data, message) {
        super(message);
        this.status = 400
        this.data = data
    }

    assert () {
        if (!this.data) {
            throw this
        }
    }
}

module.exports = BadRequestValidator