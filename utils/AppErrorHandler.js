module.exports = () => async (ctx, next) => {
    try {
        await next();
    } catch (err) {
        const {status = 500, message = 'Internal Server Error'} = err
        ctx.status = status
        ctx.body = message
    }
}