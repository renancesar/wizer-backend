let connection
const mongoose = require('mongoose')
const app = require('./app')
const request = require('supertest')

module.exports = {
    async createConnection () {
        connection = await mongoose.connect(process.env.MONGO_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        })
    },

    async closeConnection () {
        await connection.close();
    },

    createApi () {
        return request(app.callback())
    }

}