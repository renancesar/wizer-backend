# Wizer Backend

[![Codeship Status for renancesar/wizer-backend](https://app.codeship.com/projects/dd2c5320-9221-0138-2581-06abe9ebc0be/status?branch=master)](https://app.codeship.com/projects/400138)



> URL shortener using Node.js and MongoDB.

## Quick Start

```
$ git clone git@gitlab.com:renancesar/wizer-backend.git
$ cd wizer-backend
$ npm install
$ node index
```

## Aditional informations

By default the shortened url's time to life is 3600 seconds. To change this, change the value of the environment variable ``SHORTEN_TTL``

By default the mongodb url's ``mongodb://localhost``. To change this, change the value of the environment variable ``MONGO_URL``

Sample Application Running: https://wiser-backend-shorten.herokuapp.com/ 

## RESTful API

``POST /encurtador``  with body:

```
{
    url: 'https://wisereducacao.com/'
}
```

Response Sample :

```
{
    newUrl: "wiser-backend-shorten.herokuapp.com/q7xjkn25"
}
```

``GET /:hash`` With hash returned on POST request.

This route, if the hash is valid, will redirect you to the corresponding website.


If the hash is invalid, an HTTP 404 error will be returned.

## Tests

To run the test suite, first install the dependencies, then run npm test:

```
$ npm install
$ npm test
```
