const Koa = require( 'koa')
const KoaHelmet = require( 'koa-helmet')
const KoaBody = require( 'koa-body')
const Kcors = require( 'kcors')
const ShortenRoutes = require( './api/shorten/routes')
const AppErrorHandler = require('./utils/AppErrorHandler')

const App = new Koa()
    .use(AppErrorHandler())
    .use(KoaHelmet())
    .use(KoaBody({multipart: true}))
    .use(Kcors({
        methods: ['POST', 'GET', 'PUT', 'DELETE']
    }))
    .use(ShortenRoutes.routes())

module.exports = App